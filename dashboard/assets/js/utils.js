/*
* Rivets formatters
*/
rivets.formatters.join = function() {
  return [].join.call(arguments, '')
}


/*
* Rivets binders
*/
rivets.binders['cloak'] = function(el) {
  el.removeAttribute('rv-cloak')
}

rivets.binders.src = function(el, value) {
  var base = el.getAttribute('base')
  el.style.opacity = 0
  el.style.transition = 'opacity .2s'
  el.onload = function() { el.style.opacity = 1 }
  el.src = (base ? base : '') + value
}

rivets.binders.class = function(el, value) {
  value && el.classList.add(value)
}


/*
* Rivets components
*/
rivets.components['autosize-textarea'] = {
  static: ['name'],
  template: function() {
    return '<textarea rv-value="value" rv-on-keydown="autosize" '+
      'style="width:100%;min-height:120px;" rv-name="name"></textarea>'
  },
  initialize: function(el, props) {
    props.autosize = function() {
      var self = this
      setTimeout(function() {
        self.style.height = 'auto'
        self.style.height = self.scrollHeight + 'px'
      }, 0)
    }
    props.autosize.call(el.querySelector('textarea'))
    return props
  }
}


/*
* Rivets router
*/
;(function() {
  var RE1 = /^\/|\/$/g,
    RE2 = /:([^/\*\(\)\?]+)/g,
    RE3 = /\.\./g,
    RE4 = /^#?\/?|\/$/g,
    RE5 = /<template>([\s\S]+)<\/template>/,
    RE6 = /<script>([\s\S]+)<\/script>/,
    RE7 = /\(/g,
    ROUTER_LINK_ACTIVE = 'router-link-active',
    routes, element, lastView, lastComponent

  function parse(_routes) {
    routes = (_routes || []).map(function(route) {
      route.params = []
      route.path = route.path
        .replace(RE1, '')
        .replace(RE2, function(str, match) {
          route.params.push(match)
          return '([^/]+?)'
        })
        .replace(RE3, function() { return '.*' })
      route.path = new RegExp("^" + route.path + "$")
      return route
    })
  }

  function request(url, cb) {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function() {
      if (xhr.status >= 200 && xhr.status < 400)
        cb && cb(xhr.responseText)
    }
    xhr.send()
  }

  function load(name, cb) {
    if (!name) return false
    if (rivets.components[name]) {
      cb(rivets.components[name])
    } else {
      request('views/' + name.toLowerCase() + '.html',
        function(resp) {
          var tpl = RE5.exec(resp),
            scr = RE6.exec(resp)
          tpl = tpl ? tpl[1] : ''
          scr = scr ? scr[1] : ''
          rivets.components[name] = {
            template: function() { return tpl },
            initialize: new Function('$el', '$props', scr)
          }
          cb(rivets.components[name])
        })
    }
  }

  function pop(event) {
    event && event.preventDefault()
    var hash = location.hash.replace(RE4, ''),
      i = routes.length, matches
    while (i--) {
      if (matches = routes[i].path.exec(hash)) {
        if (routes[i].redirect) {
          location.hash = '#' + routes[i].redirect
          break
        }
        var name = routes[i].component, params = {}
        for (var l = 0; l < routes[i].params.length; l++) {
          params[routes[i].params[l]] = matches[l + 1]
        }
        if (routes[i].method) {
          routes[i].method()
        }
        load(routes[i].component, function(component) {
          if (lastComponent === component) {
            for (var prop in params) {
              lastComponent.$params[prop] = params[prop]
            }
          } else {
            var view = rivets.init(name, null, params)
            if (lastView) lastView.unbind()
            while (element.firstChild) {
              element.removeChild(element.firstChild)
            }
            while (view.els[0].firstChild) {
              element.appendChild(view.els[0].firstChild)
            }
            lastView = view
            lastComponent = rivets.components[routes[i].component]
            lastComponent.$params = params
            window.scrollTo(0, 0)
          }
        })
        break;
      }
    }
    checkActiveAnchors()
  }

  function each(src, itr) {
    for (var i = 0, ln = src.length; i < ln; i++)
      if (itr(src[i], i) === false) break
  }

  function toggleClass(el, cls, add) {
    var empty = !el.className,
      list = empty ? [] : el.className.split(" "),
      index = empty ? -1 : list.indexOf(cls)
    if (add) {
      if (empty || list.indexOf(cls) === -1) list.push(cls)
    } else {
      if (!empty && index !== -1) list.splice(index, 1)
    }
    el.className = list.join(" ")
  }

  function checkActiveAnchors() {
    var hash = location.hash.replace(RE4, '')
    each(document.querySelectorAll('a'), function(a) {
      var href = a.getAttribute('href')
      if (href.indexOf('#') === 0) {
        href = href.replace(RE4, '')
        toggleClass(a, ROUTER_LINK_ACTIVE, a.hasAttribute('exact') ?
          hash === href : hash.indexOf(href) === 0)
      }
    })
  }

  rivets.components['router-view'] = {
    template: function() { return '' },
    initialize: function(el, data) {
      element = el
      parse(data.routes)
      window.addEventListener('popstate', pop)
      pop()
    },
    unbind: function() {
      window.removeEventListener('popstate', pop)
    }
  }
})()


/*
* Utils
*/
function slugify(text) {
  return text.toString()
    .toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w\-]+/g, '')
    .replace(/\-\-+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');
}

function truncate(str, length, end) {
  if ((length == null) || (length >= str.length)) {
    return str
  }
  end = end || ''
  return str.slice(0, Math.max(0, length - end.length)) + end
}

function cloneObject(obj) {
  return JSON.parse(JSON.stringify(obj))
}

function date(format, time) {
  var date = time ? new Date(time) : new Date(),
    y = date.getFullYear(),
    m = date.getMonth() + 1,
    d = date.getDate(),
    h = date.getHours(),
    i = date.getMinutes(),
    s = date.getSeconds()
  return format
    .replace('y', y)
    .replace('m', m > 9 ? m : '0' + m)
    .replace('d', d > 9 ? d : '0' + d)
    .replace('h', h > 9 ? h : '0' + h)
    .replace('i', i > 9 ? i : '0' + i)
    .replace('s', s > 9 ? s : '0' + s)
}
