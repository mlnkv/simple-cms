var app = {

  state: {
    loader: true,
    wait: false,
    user: null,
    signin: {
      login: null,
      password: null
    }
  },

  modal: {
    visible: false,
    html: '',
    onCancel: null,
    onSubmit: null
  },

  messages: [],

  routes: [
    { path: '', component: 'products' },
    { path: 'product/:id', component: 'product' },
    { path: 'articles', component: 'articles' },
    { path: 'orders', component: 'orders' },
    { path: 'views', component: 'views' },
    { path: 'settings', component: 'settings' },
    {
      path: 'logout',
      method: function() {
        firebase.auth()
        .signOut()
        .then(function() {
          location.hash = ''
          location.reload()
        }, function(error) {})
      }
    }
  ],

  signIn(e) {
    e.preventDefault()
    app.state.wait = true
    firebase.auth()
      .signInWithEmailAndPassword(
        app.state.signin.login,
        app.state.signin.password
      )
      .then(function(arguments) {
        app.state.wait = false
      })
      .catch(function(error) {
        app.state.wait = false
        app.notify(error.message, 'danger')
      })
  },

  removeMessage(event, model) {
    event.target.parentNode.classList.add('hide')
    setTimeout(function() {
    event.target.parentNode.classList.remove('hide')
      app.messages.splice(model.index, 1)
    }, 400)
  },

  notify(text, type) {
    app.messages.push({
      text: text,
      type: type ? 'banner-' + type : ''
    })
  },

  message(html, onSubmit, onCancel) {
    app.modal.html = html
    app.modal.visible = true
    app.modal.onSubmit = onSubmit
    app.modal.onCancel = onCancel
  },

  onMessageSubmit() {
    app.modal.visible = false
    if (typeof app.modal.onSubmit == 'function')
      app.modal.onSubmit()
    app.modal.onSubmit = null
    app.modal.onCancel = null
  },

  onMessageCancel() {
    app.modal.visible = false
    if (typeof app.modal.onCancel == 'function')
      app.modal.onCancel()
    app.modal.onSubmit = null
    app.modal.onCancel = null
  }
}


rivets.bind(document.querySelector('#app'), app)

firebase.initializeApp({
  apiKey: 'AIzaSyBtgCTQYeRKrwoCKCXhR4bXhdwmhtUFL5E',
  databaseURL: 'https://chiffon-94fba.firebaseio.com',
  storageBucket: 'chiffon-94fba.appspot.com'
})

firebase.auth().onAuthStateChanged(function(user) {
  app.state.user = user
  app.state.loader = false
})